﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using EFGetStarted.AspNetCore.NewDb.Models;

namespace EFGetStarted.AspNetCore.NewDb.Models
{
    public class ModelContext : DbContext
    {
        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        { }

        public DbSet<Model> Modele { get; set; }
    }

    public class Model
    {
        public int ID { get; set; }
        public string Liczba { get; set; }
        public string Odpowiedz { get; set; }
        public string IP { get; set; }
        public void fizzbuzz(string nazwa)
        {
            string temp = "";
            if (int.TryParse(nazwa, out int x))
            {
                if (x < 1 || x > 100)
                {
                    Odpowiedz = "Liczba nie jest z przedziału 1-100";
                }
                else
                {
                    if (x % 3 == 0)
                    {
                        temp = "Fizz";
                    }
                    if (x % 5 == 0)
                    {
                        temp = temp + "Buzz";
                    }
                    if (x % 7 == 0)
                    {
                        temp = temp + "Wizz";
                    }
                    if (temp == "")
                    {
                        Odpowiedz = nazwa;
                    }
                    else Odpowiedz = temp;

                }  
            }
            else
            {
                Odpowiedz = "Podano tekst lub puste pole";
            }
        }
    }
    
}