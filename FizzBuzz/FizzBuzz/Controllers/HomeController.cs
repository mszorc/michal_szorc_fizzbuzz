﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFGetStarted.AspNetCore.NewDb.Models;
using Microsoft.AspNetCore.Http;

namespace FizzBuzz.Controllers
{
    public class HomeController : Controller
    {
        private readonly ModelContext _context;
        private IHttpContextAccessor _accessor;
        public HomeController(ModelContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _accessor = accessor;
            _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }
        // GET: Models
        public async Task<IActionResult> Index()
        {
            
            return View(await _context.Modele.ToListAsync());
        }

        // GET: Models/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Models/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Liczba,Odpowiedz,IP")] Model model)
        {
            if (ModelState.IsValid)
            {
                model.fizzbuzz(model.Liczba);
                //_accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                model.IP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                _context.Add(model);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(model);
        }

        // POST: Models/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.


        // GET: Models/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var model = await _context.Modele
                .FirstOrDefaultAsync(m => m.ID == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        // POST: Models/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var model = await _context.Modele.FindAsync(id);
            _context.Modele.Remove(model);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ModelExists(int id)
        {
            return _context.Modele.Any(e => e.ID == id);
        }
    }
}
