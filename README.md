1. Author - Michal Szorc
2. Link to repository - https://gitlab.com/mszorc/michal_szorc_fizzbuzz
3. Version of .NET used: ASP.NET CORE 2.2 MVC
4. Operating system: Windows 10 Home  1803  Kompilacja 17134.648
6. Data base - SQL Server 13.0.4001
7. Manual:
* Start FizzBuzz.sln file in Visual Studio.
* Use ctrl+f5 key combination to compile program. Website will automatically open in the browser.
